#!/bin/bash
set -euo pipefail

# composer更新
cd /var/www/app
if [ -f "composer.lock" ]; then
   /usr/local/bin/composer update -n
else
   /usr/local/bin/composer install -n
fi

# laravel
sudo chmod -R 777 /var/www/app/storage
